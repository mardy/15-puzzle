import QtQuick 2.0

Image {
    id: root

    signal clicked()

    width: buttonSize
    height: width
    fillMode: Image.PreserveAspectFit
    smooth: true

    MouseArea {
        anchors.fill: parent
        onClicked: root.clicked()
    }
}

import QtQuick 2.0

ListModel {
    id: root

    property int columns: 4
    property int rows: 4

    signal solved()

    property bool __scrambling: false

    Component.onCompleted: {
        var size = columns * rows
        for (var i = 0; i < size; i++) {
            append({
                "n": i,
                "empty": (i == size - 1),
            })
        }
        scramble()
    }

    function indexOf(n) {
        for (var i = 0; i < count; i++) {
            var item = get(i)
            if (item.n == n) return i
        }
        return -1
    }

    function findEmpty() {
        return indexOf(count - 1)
    }

    function posFromIdx(idx) {
        return {
            "col": idx % columns,
            "row": Math.floor(idx / columns),
        }
    }

    function idxFromPos(row, col) {
        return row * columns + col
    }

    function swap(i1, i2) {
        var diff = i2 - i1
        if (diff > 1) {
            move(i1, i2 - 1, 1)
            move(i2, i1, 1)
        } else if (diff < -1) {
            move(i1, i2, 1)
            move(i2 + 1, i1, 1)
        } else {
            move(i1, i2, 1)
        }
        if (!__scrambling) {
            checkSolved()
        }
    }

    function shiftCell(idx) {
        var empty = findEmpty()
        var pos = posFromIdx(idx)
        var emptyPos = posFromIdx(empty)
        var colDiff = emptyPos.col - pos.col
        var rowDiff = emptyPos.row - pos.row
        var idx = pos.row * columns + pos.col
        var emptyIdx = emptyPos.row * columns + emptyPos.col
        if (colDiff == 0) {
            if (rowDiff == 1 || rowDiff == -1) {
                swap(idx, emptyIdx)
                return true
            }
        } else if (rowDiff == 0) {
            if (colDiff == 1 || colDiff == -1) {
                swap(idx, emptyIdx)
                return true
            }
        }
        return false
    }

    function rand(from, to) {
        return Math.floor(Math.random() * (to - from + 1)) + from
    }

    function getAdjacentCells(idx){
        var pos = posFromIdx(idx)
        var row = pos.row
        var col = pos.col
        var adjacent = []
        // Gets all possible adjacent cells
        if (row < rows - 1) { adjacent.push(idxFromPos(row+1, col)) }
        if (row > 0) { adjacent.push(idxFromPos(row-1, col)) }
        if (col < columns - 1) { adjacent.push(idxFromPos(row, col+1)) }
        if (col > 0) { adjacent.push(idxFromPos(row, col-1)) }
        
        return adjacent;
        
    }

    function scramble() {
        __scrambling = true
        var previousCell
        for (var i = 0; i < 100; i++) {
            var empty = findEmpty()
            var adjacent = getAdjacentCells(empty)
            if (previousCell) {
                var j = adjacent.indexOf(previousCell)
                if (j >= 0) {
                    adjacent.splice(j, 1)
                }
            }

            previousCell = adjacent[rand(0, adjacent.length-1)]
            shiftCell(previousCell)
        }
        __scrambling = false
    }

    function checkSolved() {
        var size = columns * rows
        for (var i = 0; i < size; i++) {
            if (get(i).n != i) return false
        }
        solved()
        return true
    }
}

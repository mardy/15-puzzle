import QtQuick 2.0

Flipable {
    id: root

    property url imageFile
    property alias columns: tileModel.columns
    property alias rows: tileModel.rows

    width: Math.min(parent.width, parent.height)
    height: width

    TileModel {
        id: tileModel
    }

    front: Board {
        id: board
        anchors { left: parent.left; right: parent.right }
        tileModel: tileModel
    }
    back: Image {
        id: fullImage
        anchors.fill: board
        sourceSize.width: width
        smooth: true
        fillMode: Image.PreserveAspectCrop
        source: root.imageFile
        asynchronous: true
        onStatusChanged: if (status == Image.Ready) {
            fullImage.grabToImage(function(result) {
                board.boardImage = result.url
            })
        }
    }

    transform: Rotation {
        id: rotation
        origin.x: root.width / 2
        origin.y: root.height / 2
        axis { x: 0; y: 1; z: 0 }

        Behavior on angle {
            NumberAnimation { duration: 500 }
        }
    }

    function flip() {
        rotation.angle = (side == Flipable.Front) ? 180 : 0
    }
}

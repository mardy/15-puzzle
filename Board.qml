import QtQuick 2.0

Rectangle {
    id: board

    property alias tileModel: repeater.model
    property url boardImage

    height: width

    color: "#411"

    Grid {
        id: grid

        property int tileSize: width / columns

        anchors.fill: parent
        columns: tileModel ? tileModel.columns : 0
        move: Transition {
            NumberAnimation { properties: "x,y"; easing.type: Easing.OutQuad }
        }

        Repeater {
            id: repeater
            Tile {
                id: tile
                width: board.width / grid.columns
                n: model.n
                source: board.boardImage
                pos: tileModel.posFromIdx(model.n)
                empty: model.empty
                onClicked: grid.shiftCell(tile)
            }
        }

        function cellIndex(cell) {
            return tileModel.indexOf(cell.n)
        }

        function shiftCell(cell) {
            tileModel.shiftCell(cellIndex(cell))
        }
    }
}

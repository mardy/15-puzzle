import QtQuick 2.0

Item {
    id: root

    property url imageFile: "./ubuntu-logo.jpg"
    property int buttonSize: Math.min(width, height) / 5

    width: 500
    height: 800

    Background { id: background }

    Button {
        anchors { top: parent.top; right: parent.right; margins: 10 }
        source: "./icons/show-picture.png"
        onClicked: game.flip()
    }

    Game {
        id: game
        anchors.centerIn: parent
        imageFile: root.imageFile
        columns: 4
        rows: 4
    }
}

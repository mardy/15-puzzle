import QtGraphicalEffects 1.0
import QtQuick 2.0

Rectangle {
    id: root

    anchors.fill: parent
    gradient: Gradient {
        GradientStop { position: 0.0; color: "#7cbdff" }
        GradientStop { position: 1.0; color: "#7cff7f" }
    }
}

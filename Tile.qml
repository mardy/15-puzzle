import QtQuick 2.0

Item {
    id: root

    property int n: -1
    property var pos: null
    property bool empty: false
    property alias source: image.source

    signal clicked()

    height: width

    Rectangle {
        anchors.fill: parent
        visible: !empty
        border { width: 1; color: "#222" }
        color: "white"
        clip: true
        Text {
            anchors.fill: parent
            visible: image.status != Image.Ready
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            text: n + 1
        }

        Image {
            id: image
            x: -pos.col * root.width
            y: -pos.row * root.height
        }

        MouseArea {
            anchors.fill: parent
            onClicked: root.clicked()
        }
    }
}
